# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""


import pandas as pd
import numpy as np

df = pd.read_csv("chess.csv")

# Task 1 - remove "jan" from date and convert into integer
df.Date = df.Date.str.replace("Jan", "").astype(int)
# -------

# rename "Position" column
df.columns = ['Position', 'Name', 'ELO', 'Date', 'Age']

# Task 2 - find the highest ELO and find its year

# df.loc[df.ELO.argmax()].Date 
# is wrong since it will provide single value 
# the data may have multiple maximums !

df.loc[df.ELO == df.ELO.max()].Date
# -------

# Task 3 - Find the average of top 20 ELO and time trend

# list comprehension method
x1=[df.loc[df.Date==2000+i].ELO.mean() for i in range(22)]
x2=[df.loc[df.Date==i].ELO.mean() for i in range(2000, 2022)]

# better approach
x3=[df.loc[df.Date==d].ELO.mean() for d in df.Date.unique()]

# pandas groupby method
x4 = df.groupby("Date")["ELO"].mean()

# plot
#x4.plot()

# Task 4 - are the player getting stronger, weaker or significant differences
x5 = df.sort_values(["Date"]).groupby("Name")

# task 5 - player having minimum ELO who ever appeared in top 20
x6 = df.groupby("Date")["ELO"].min().min()
df.loc[df.ELO == x6].Name

#task 6 - top 20 average age year wise and time trend

x7 = df.groupby("Date")["Age"].mean()
x7.plot()
